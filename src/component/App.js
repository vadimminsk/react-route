import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import About from './About'
import Home from './Home'
import Contact from './Contact'
import Portfolio from './Portfolio'
import {Button, Icon} from 'react-materialize'

const BasicExample = () => (
    <Router>
        <div>
            <ul>
                <li>
                    <Button ><Link to="/">Home</Link></Button>
                </li>
                <li>
                    <Button ><Link to="/about">About</Link></Button>
                </li>
                <li>
                    <Button ><Link to="/contact">Contact</Link></Button>
                </li>
                <li>
                    <Button ><Link to="/portfolio">Portfolio</Link></Button>
                </li>
            </ul>

            <hr />

            <Route exact path="/" component={Home} />
            <Route path="/about" component={About} />
            <Route path="/contact" component={Contact} />
            <Route path="/portfolio" component={Portfolio} />
        </div>
    </Router>
);

// const HomePage = () => (
//     <div>
//         <h2 >Home</h2>
//     </div>
// );
//
// const AboutPage = () => (
//     <div>
//         <h2>About</h2>
//     </div>
// );
//
// const PorfolioPage = () => (
//     <div>
//         <h2>Portfolio</h2>
//     </div>
// );
// const ContactPage = () => (
//     <div>
//         <h2>Contact</h2>
//     </div>
// );



export default BasicExample;
